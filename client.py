import asyncio
import websockets
import json
import random
import aioconsole

# For testing
USERNAMES = [
    "user1", "john_doe", "jane_doe", "admin", "guest", "root", "test_user", 
    "anonymous", "new_user", "username123", "user123", "test", "member", 
    "player1", "player2", "charlie", "alice", "bob", "eve", "sam", "alex", 
    "mike", "sara", "chris", "pat", "kim", "lee", "robin", "casey", "morgan", 
    "taylor", "jordan", "dylan", "jamie", "riley", "skyler", "drew", 
    "avery", "kyle", "jules", "dev", "nico", "rowan", "milan", "quinn", 
    "fin", "bailey", "blake", "angel", "reese", "corey", "jaden", 
    "peyton", "kai", "sloan", "tatum", "logan", "cameron", "kerry", 
    "alexis", "micah", "rory", "case", "river", "sage", "jules", 
    "jayden", "harley", "reed", "finley", "shay", "mckenzie", 
    "blair", "teagan", "dana", "parker", "toby", "ryan", "frankie", 
    "ashton", "morgan", "ari", "skylar", "phoenix", "remi", "hollis", 
    "lennox", "kit", "sydney"
]

async def send_init_message(ws, username):
    message = {
        "init": 1,
        "username": username
    }
    message_str = json.dumps(message)
    await ws.send(message_str)

async def send_message(ws, message):
    json_message = {
        "message": message
    }
    message_str = json.dumps(json_message)
    await ws.send(message_str)

async def receive_messages(ws):
    while True:
        response = await ws.recv()
        print(f"Message received: {str(response)[2:-1]}")

async def send_user_input(ws):
    while True:
        message = await aioconsole.ainput("")
        await send_message(ws, message)

async def connect():
    uri = "ws://10.30.30.114:1337"
    async with websockets.connect(uri) as ws:
        try:
            # Send init message
            await send_init_message(ws, random.choice(USERNAMES))

            receive_task = asyncio.create_task(receive_messages(ws))
            send_task = asyncio.create_task(send_user_input(ws))

            await asyncio.gather(receive_task)
            await asyncio.gather(receive_task, send_task)
        except websockets.ConnectionClosed:
            print("Connection closed")

def main():
    asyncio.get_event_loop().run_until_complete(connect())


if __name__ == '__main__':
    main()
